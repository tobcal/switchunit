#include <FS.h>                   //if we want to save settings
#include "Arduino.h"
#include <WiFiClient.h>
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include "WiFiManager.h"         //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>

int RELAY = 4;
char email_address[100];
char mac_address[12];
String mac_str;
//flag for saving data
bool shouldSaveConfig = false;
ESP8266WebServer httpServer(80);
//ESP8266HTTPUpdateServer httpUpdater;
void setup() {

  Serial.begin(115200);
  readMacAddress();
  SPIFFS.format();
  readSettings();
  pinMode(RELAY, OUTPUT);

  // Get stuff from server like sleeptime and if it should update
  
  
  //setupRemoteUpdate();

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
  WiFiManagerParameter custom_mac_address("mac_address", "mac_address", mac_address, 12);
  WiFiManagerParameter custom_email_address("email_address", "Din email-adress", email_address, 100);

  WiFiManager wifiManager;

  //set config save notify callback
  //wifiManager.setSaveConfigCallback(saveConfigCallback);

  //set static ip
  //wifiManager.setSTAStaticIPConfig(IPAddress(10, 0, 1, 99), IPAddress(10, 0, 1, 1), IPAddress(255, 255, 255, 0));

  // Custom parameters
  wifiManager.addParameter(&custom_mac_address);
  wifiManager.addParameter(&custom_email_address);
  //delay(1000);

  //reset settings - for testing
  //wifiManager.resetSettings();

  //set minimu quality of signal so it ignores AP's under that quality
  //defaults to 8%
  //wifiManager.setMinimumSignalQuality();

  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  //wifiManager.setTimeout(120);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //and goes into a blocking loop awaiting configuration
  //if (!wifiManager.startConfigPortal("Level", "password")) {
  if (!wifiManager.autoConnect("Level", "password")) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...");

  //read updated parameters
  strcpy(email_address, custom_email_address.getValue());
  strcpy(mac_address, custom_mac_address.getValue());

  if (shouldSaveConfig) {
    saveSettings();
  }
  Serial.println("local ip");
  Serial.println(WiFi.localIP());

  if ((WiFi.status() == WL_CONNECTED)) {
    String emailString = String(email_address);

    HTTPClient http;
    http.begin("http://levelweb.azurewebsites.net/api/Account");
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST("{'userName': '" + emailString + "', 'mACAddress': '" + mac_str + "'}");
    Serial.println("{'userName': '" + emailString + "', 'mACAddress': '" + mac_str + "'}");

    if (httpCode > 0 && httpCode != 400) {
      Serial.printf("Request success, code: %d\n", httpCode);
      if (httpCode == 201) { // Created user
        String payload = http.getString();
        Serial.println(payload);
      }
      else { // User already exists?
        //Serial.printf("Request failed, error: %s\n", http.errorToString(httpCode).c_str());
        Serial.println(httpCode);
      }
    }
    else {
      //Serial.printf("Request failed, error: %s\n", http.errorToString(httpCode).c_str());
      Serial.println(httpCode);
    }
    http.end();
  }
}

void loop() {

  // Directly connect to API to check what on/off status is set for this MACADRESS
  httpServer.handleClient();
  
  HTTPClient http;
  http.begin("http://levelweb.azurewebsites.net/api/LevelApi/GetRelayStatus?macAddress="+mac_str); //HTTP
  //http.addHeader("Content-Type", "application/json");
  Serial.println("Sending: {'nodeMACAddress': '" + mac_str + "'}");
  //int httpCode = http.POST("{'nodeMACAddress': '" + mac_str + "'}");
  int httpCode = http.GET();

  // httpCode will be negative on error
  if (httpCode > 0 && httpCode != 400) {
    String response = http.getString();
    Serial.printf("Request success, code: %d\n", httpCode);
    Serial.println(response);
    if (httpCode == 200) { // If value was retrieved
      
      Serial.println(response);
      
      // Parse response to see if relay should be turned on
      if (response.equals("SETRELAYON"))
      {
        digitalWrite(RELAY, HIGH);
        Serial.println("on");
      }
      else
      {
        digitalWrite(RELAY, LOW);
        Serial.println("off");
      }
    }
    else {
      //
      Serial.printf(http.errorToString(httpCode).c_str());
    }
  }
  else {
    Serial.printf("Request failed, error: %s\n", http.errorToString(httpCode).c_str());
  }
  http.end();

  delay(5000);
  
  
}

void readMacAddress()
{
  uint8_t MAC_array[6];
  WiFi.macAddress(MAC_array);
  for (int i = 0; i < sizeof(MAC_array); ++i) {
    sprintf(mac_address, "%s%02x", mac_address, MAC_array[i]);
  }
  mac_str = String(mac_address);
}

void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

