  // Filesystem stuff
  //clean FS, for testing
  //SPIFFS.format();

  //read configuration from FS json
  //Serial.println("mounting FS...");
  //Callback notifying us of the need to save config
  

  void readSettings() {
    if (SPIFFS.begin()) {
      //Serial.println("mounted file system");
      if (SPIFFS.exists("/config.json")) {
        //file exists, reading and loading
        //Serial.println("reading config file");
        File configFile = SPIFFS.open("/config.json", "r");
  
        if (configFile) {
          Serial.println("opened config file");
          size_t size = configFile.size();
          // Allocate a buffer to store contents of the file.
          std::unique_ptr<char[]> buf(new char[size]);
  
          configFile.readBytes(buf.get(), size);
          DynamicJsonBuffer jsonBuffer;
          JsonObject& json = jsonBuffer.parseObject(buf.get());
          json.printTo(Serial);
          if (json.success()) {
            Serial.println("\nparsed json");
  
            strcpy(mac_address, json["mac_address"]);
            strcpy(email_address, json["email_address"]);
  
          } 
          else {
            Serial.println("failed to load json config");
          }
        }
      }
    } 
    else {
      Serial.println("failed to mount FS");
    }
  }
//  
  //end read

void saveSettings() {
  //save the custom parameters to FS
  Serial.println("saving config");
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
  json["email_address"] = email_address;
  json["mac_address"] = mac_address;

  File configFile = SPIFFS.open("/config.json", "w");
  if (!configFile) {
    Serial.println("failed to open config file for writing");
  }

  json.printTo(Serial);
  json.printTo(configFile);
  configFile.close();
  //end save
}

