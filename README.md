﻿# LevelNodeUnit #

1. Ladda in filen LevelNodeUnit.ino i Arduini IDE för att öppna "projektet"
(Det innehåller flera filer).

(Du kan behöva ladda ner biblioteket ArduinoJson "Sketch -> Include Library -> Manage Libraries...")

2. Kompilera och ladda upp koden till enheten.

3. Koppla upp dig till WiFi-nätverket med ssid "Level".

4. Ett webbläsarfönster ska öppnas. Välj wifi-nätverk, skriv i lösenord till nätverket, skriv i en mailaddress.